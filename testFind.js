const find = require(`./find.js`)

let arr = [1,2,3,4,5,5]
let cb = function(x,i){return x>2}
let result = find(arr,cb)
console.log(result)

arr = [1,2,3,4,5,5]
cb = function(x,i){ return x+=2}
result = find(arr,cb)
console.log(result)

arr = []
cb = function(x,i){return true}
result = find(arr,cb)
console.log(result)

arr = 'abc'
cb = function(x,i){return true}
result = find(arr,cb)
console.log(result)

arr = [1,2,3,4,5,5]
cb = 'abc'
result = find(arr,cb)
console.log(result)

