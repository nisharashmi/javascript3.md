function reduce(arr,cb,startingValue){
    if(!arr || !cb){
        return undefined;
    }
    let i=0;
    if(!startingValue){
        startingValue = arr[0];

    }
    for(i=1;i<arr.length;i++){
        startingValue = cb(startingValue,arr[i])

    }
    return startingValue;
}
module.exports = reduce;