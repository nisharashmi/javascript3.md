function find(arr,cb){
    if(!arr || !cb){
        return undefined
    }
    for(let i=0;i<arr.length;i++){
        if(cb(arr[i])){
            return arr[i]
        }
    }
    return undefined;
}
module.exports = find