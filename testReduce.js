const reduce = require(`./reduce.js`)

let arr = [1,2,3,4,5,5]
let initialVal = 0
let cb = function(startingValue , x){return startingValue+x}
let result = reduce(arr,cb,initialVal)
console.log("1. "+result)

arr = []
initialVal = 0
cb = function(startingValue,x){return startingValue+x}
result = reduce(arr,cb,initialVal)
console.log("2. "+result)

arr = [1,2,3,4,5,5]
cb = function(startingValue,x){return startingValue+x}
result = reduce(arr,cb)
console.log("3. "+result)

cb = function(startingValue,x){return startingValue+x}
result = reduce(cb)
console.log("4. "+result)

result = reduce(arr)
console.log("5. "+result)