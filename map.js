function map(arr,cb){

    if(!arr || !cb){
        return []
    }
    let newArr = []
    for(let i=0;i<arr.length;i++){
        let newVal = cb(arr[i],i,arr);
        newArr.push(newVal);

    }
    return newArr;
}module.exports = map