const filter = require(`./filter.js`)

let arr = [1,2,3,4,5,5]
let cb = function(x,i){return x>2}
let result = filter(arr,cb)
console.log(result)

arr = [1,2,3,4,5,5]
cb = function(x,i){ return x+=2}
result = filter(arr,cb)
console.log(result)

arr = []
cb = function(x,i){return true}
result = filter(arr,cb)
console.log(result)

arr = 'abc'
cb = function(x,i,arr){return true}
result = filter(arr, cb)
console.log(result)

arr = [1,2,3,4,5,5]
cb = 'abc'
result = filter(arr,cb)
console.log(result)