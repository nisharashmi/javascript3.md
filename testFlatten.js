const flatten = require(`./flatten`);


let arr = [1, [2], [[3]], [[[4]]]];
let result = flatten(arr);
console.log("1. " + result)

arr = []
result = flatten(arr);
console.log("2. " +result)

arr = [1,2,3]
result = flatten(arr);
console.log("3. " +result)

arr = { 'a': 2, 'b': 3, 'c':4}
result = flatten(arr);
console.log("4. " + result)