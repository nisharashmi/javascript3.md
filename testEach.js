const each = require(`./each.js`)

let arr = [1, 2, 3, 4, 5 ,5]
let cb = function(x,i){console.log(x)}
each(arr.cb)
arr = {'val1':2,'val2':8}
cb = function(x,i){console.log(x)}
each(arr,cb)
arr = []
cb = function(x,i){console.log(x)}
each(arr,cb)
arr = [1, 2, 3, 4, 5, 5];
cb = null
each(arr.cb)
arr=undefined
cb = function(x,i){console.log(x)}
each(arr,cb)